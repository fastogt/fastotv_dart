import 'package:fastotv_dart/utils.dart';

class SubscriberSignUp {
  static const _EMAIL_FIELD = 'email';
  static const _PASSWORD_FIELD = 'password';
  static const _FIRST_NAME_FIELD = 'first_name';
  static const _LAST_NAME_FIELD = 'last_name';
  static const _COUNTRY_FIELD = 'country';
  static const _LANGUAGE_FIELD = 'language';

  String email;
  String password;
  String firstName;
  String lastName;
  String country;
  String language;

  static const MAX_NAME_LENGTH = 64;
  static bool isValidName(String name) {
    return name.isNotEmpty && name.length < MAX_NAME_LENGTH;
  }

  bool isValid() {
    final bool fields = email.isNotEmpty &&
        isValidName(firstName) &&
        isValidName(lastName) &&
        language.isNotEmpty &&
        country.isNotEmpty;

    return fields && password.isNotEmpty;
  }

  SubscriberSignUp(
      {required this.email,
      required this.password,
      required this.firstName,
      required this.lastName,
      required this.country,
      required this.language});

  SubscriberSignUp.create(
      {required this.email,
      required this.password,
      required this.firstName,
      required this.lastName,
      required this.country,
      required this.language});

  SubscriberSignUp.createDefault()
      : email = '',
        firstName = '',
        lastName = '',
        password = '',
        country = '',
        language = '';

  factory SubscriberSignUp.fromJson(Map<String, dynamic> json) {
    final email = json[_EMAIL_FIELD];
    final firstName = json[_FIRST_NAME_FIELD];
    final lastName = json[_LAST_NAME_FIELD];
    final password = json[_PASSWORD_FIELD];
    final country = json[_COUNTRY_FIELD];
    final language = json[_LANGUAGE_FIELD];

    return SubscriberSignUp(
        email: email,
        password: password,
        firstName: firstName,
        lastName: lastName,
        country: country,
        language: language);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      _EMAIL_FIELD: email.toLowerCase().trim(),
      _FIRST_NAME_FIELD: firstName,
      _LAST_NAME_FIELD: lastName,
      _PASSWORD_FIELD: password,
      _COUNTRY_FIELD: country,
      _LANGUAGE_FIELD: language
    };
    return result;
  }
}

class SubscriberSignUpFront extends SubscriberSignUp {
  static const _PHONE_FIELD = 'phone';
  static const _DETAILS_FIELD = 'details';

  String? phone;
  String? details;

  SubscriberSignUpFront(
      {required String email,
      required String password,
      required String firstName,
      required String lastName,
      required String country,
      required String language,
      this.phone,
      this.details})
      : super(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            country: country,
            language: language);

  SubscriberSignUpFront.createDefault()
      : phone = null,
        details = null,
        super(email: '', password: '', firstName: '', lastName: '', country: '', language: '');

  factory SubscriberSignUpFront.fromJson(Map<String, dynamic> json) {
    final base = SubscriberSignUp.fromJson(json);

    String? phone;
    if (json.containsKey(_PHONE_FIELD)) {
      phone = json[_PHONE_FIELD];
    }

    String? details;
    if (json.containsKey(_DETAILS_FIELD)) {
      details = json[_DETAILS_FIELD];
    }

    return SubscriberSignUpFront(
        email: base.email,
        password: base.password,
        firstName: base.firstName,
        lastName: base.lastName,
        country: base.country,
        language: base.language,
        phone: phone,
        details: details);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> base = super.toJson();
    if (phone != null) {
      base[_PHONE_FIELD] = phone;
    }
    if (details != null) {
      base[_DETAILS_FIELD] = details;
    }
    return base;
  }
}

class SubscriberStatus {
  static const int _NOT_ACTIVE_CONSTANT = 0;
  static const int _ACTIVE_CONSTANT = 1;
  static const int _DELETED_CONSTANT = 2;

  final int _value;

  const SubscriberStatus._(this._value);

  int compareTo(SubscriberStatus status) {
    return _value.compareTo(status._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NOT_ACTIVE_CONSTANT) {
      return 'NOT ACTIVE';
    } else if (_value == _ACTIVE_CONSTANT) {
      return 'ACTIVE';
    }

    assert(_value == _DELETED_CONSTANT);
    return 'DELETED';
  }

  factory SubscriberStatus.fromInt(int status) {
    if (status == _NOT_ACTIVE_CONSTANT) {
      return SubscriberStatus.NOT_ACTIVE;
    } else if (status == _ACTIVE_CONSTANT) {
      return SubscriberStatus.ACTIVE;
    } else if (status == _DELETED_CONSTANT) {
      return SubscriberStatus.DELETED;
    }

    throw 'Unknown subscriber status: $status';
  }

  static List<SubscriberStatus> get values => [NOT_ACTIVE, ACTIVE, DELETED];

  static const SubscriberStatus NOT_ACTIVE = SubscriberStatus._(_NOT_ACTIVE_CONSTANT);
  static const SubscriberStatus ACTIVE = SubscriberStatus._(_ACTIVE_CONSTANT);
  static const SubscriberStatus DELETED = SubscriberStatus._(_DELETED_CONSTANT);
}

class SubscriberAdd extends SubscriberSignUpFront {
  static const _CREATED_DATE_FIELD = 'created_date';
  static const _STATUS_FIELD = 'status';
  static const _MAX_DEVICE_COUNT_FIELD = 'max_devices_count';
  static const _SERVERS_FIELD = 'servers';
  static const _OWNER_FIELD = 'owner';
  static const _EXP_DATE_FIELD = 'exp_date';

  static const EXPIRE_TTL = 30 * 24 * 3600 * 1000;

  UtcTimeMsec expDate;
  SubscriberStatus status;
  int maxDevicesCount;
  List<String> servers;

  // can change created date
  final UtcTimeMsec? createdDate;

  // servers
  String? owner;

  @override
  bool isValid() {
    final bool fields = super.isValid() && expDate >= 0 && maxDevicesCount >= 0;
    return fields;
  }

  SubscriberAdd(
      {required String email,
      required String password,
      required String firstName,
      required String lastName,
      this.createdDate,
      String? phone,
      String? details,
      required this.status,
      required this.maxDevicesCount,
      required String language,
      required String country,
      required this.owner,
      required this.servers,
      required this.expDate})
      : super(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            country: country,
            language: language,
            phone: phone,
            details: details);

  SubscriberAdd.create(
      {required String email,
      required String password,
      required String firstName,
      required String lastName,
      required this.status,
      required this.maxDevicesCount,
      required String language,
      required String country,
      required this.owner,
      required this.servers,
      required this.expDate})
      : createdDate = null,
        super(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            country: country,
            language: language);

  SubscriberAdd.createDefault(int devicesCount)
      : createdDate = null,
        owner = null,
        expDate = makeUTCTimestamp() + EXPIRE_TTL,
        maxDevicesCount = devicesCount,
        status = SubscriberStatus.ACTIVE,
        servers = [],
        super.createDefault();

  SubscriberAdd copy() {
    return SubscriberAdd(
        email: email,
        firstName: firstName,
        password: password,
        lastName: lastName,
        createdDate: createdDate,
        expDate: expDate,
        status: status,
        language: language,
        country: country,
        owner: owner,
        maxDevicesCount: maxDevicesCount,
        servers: servers);
  }

  // password field not exists in json
  factory SubscriberAdd.fromJson(Map<String, dynamic> json) {
    final profile = SubscriberSignUpFront.fromJson(json);
    final status = SubscriberStatus.fromInt(json[_STATUS_FIELD]);
    final maxDeviceCount = json[_MAX_DEVICE_COUNT_FIELD];
    final List<String> servers = json[_SERVERS_FIELD].cast<String>();

    int? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    String? owner;
    if (json.containsKey(_OWNER_FIELD)) {
      owner = json[_OWNER_FIELD];
    }
    final exp = json[_EXP_DATE_FIELD];
    return SubscriberAdd(
        email: profile.email,
        password: profile.password,
        firstName: profile.firstName,
        lastName: profile.lastName,
        createdDate: createdDate,
        owner: owner,
        expDate: exp,
        status: status,
        maxDevicesCount: maxDeviceCount,
        language: profile.language,
        country: profile.country,
        servers: servers);
  }

  DateTime expiredDate() {
    final DateTime dt = DateTime.fromMillisecondsSinceEpoch(expDate);
    return dt;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();

    result[_STATUS_FIELD] = status.toInt();
    result[_MAX_DEVICE_COUNT_FIELD] = maxDevicesCount;
    result[_SERVERS_FIELD] = servers;
    result[_EXP_DATE_FIELD] = expDate;

    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    if (owner != null) {
      result[_OWNER_FIELD] = owner;
    }
    return result;
  }
}

// for players
class SubProfile extends SubscriberAdd {
  static const _ID_FIELD = 'id';
  static const _DEVICES_FIELD = 'devices';

  final String id;

  List<String> devices;

  int get devicesCount {
    return devices.length;
  }

  SubProfile(
      {required this.id,
      required String email,
      required String password,
      required String firstName,
      required String lastName,
      required String country,
      required String language,
      String? phone,
      String? details,
      required UtcTimeMsec expDate,
      required SubscriberStatus status,
      required int maxDevicesCount,
      required List<String> servers,
      required this.devices,
      UtcTimeMsec? createdDate,
      String? owner})
      : super(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            country: country,
            language: language,
            phone: phone,
            details: details,
            expDate: expDate,
            status: status,
            maxDevicesCount: maxDevicesCount,
            servers: servers,
            createdDate: createdDate,
            owner: owner);

  SubProfile copy() {
    return SubProfile(
        id: id,
        email: email,
        password: password,
        firstName: firstName,
        lastName: lastName,
        country: country,
        language: language,
        phone: phone,
        details: details,
        expDate: expDate,
        servers: servers,
        devices: devices,
        maxDevicesCount: maxDevicesCount,
        status: status,
        createdDate: createdDate,
        owner: owner);
  }

  factory SubProfile.fromJson(Map<String, dynamic> json) {
    final base = SubscriberAdd.fromJson(json);
    final id = json[_ID_FIELD];
    final List<String> devices = json[_DEVICES_FIELD].cast<String>();
    return SubProfile(
        id: id,
        email: base.email,
        password: base.password,
        firstName: base.firstName,
        lastName: base.lastName,
        expDate: base.expDate,
        createdDate: base.createdDate,
        owner: base.owner,
        country: base.country,
        language: base.language,
        status: base.status,
        servers: base.servers,
        maxDevicesCount: base.maxDevicesCount,
        details: base.details,
        phone: base.phone,
        devices: devices);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[_ID_FIELD] = id;
    result[_DEVICES_FIELD] = devices;
    return result;
  }
}
