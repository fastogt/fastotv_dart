typedef UtcTimeMsec = int;

DateTime currentDate() {
  return DateTime.now();
}

DateTime currentUTCDate() {
  final cur = currentDate();
  return cur.toUtc();
}

UtcTimeMsec dateTime2UtcTimeMsec(DateTime date) {
  final utc = date.toUtc();
  return utc.millisecondsSinceEpoch;
}

DateTime utcTimeMsec2DateTime(UtcTimeMsec msec) {
  return DateTime.fromMillisecondsSinceEpoch(msec);
}

UtcTimeMsec makeUTCTimestamp() {
  final ts = currentUTCDate();
  final utc = ts.toUtc();
  return utc.millisecondsSinceEpoch;
}
