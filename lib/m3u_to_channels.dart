import 'package:fastotv_dart/m3u_parser.dart';

const ID_FIELD = 'id';
const NAME_FIELD = 'name';
const PRIMARY_URL_FIELD = 'url';
const GROUP_FIELD = 'group';
const ICON_FIELD = 'icon';
const ATTRIBUTES_FIELD = 'attributes';

class M3UParser {
  M3UParser(this.file);

  final String file;

  static const CHANNELS_DIVIDER = '#EXTINF:-1';
  static const NAME_TAG = 'tvg-name=';
  static const ID_TAG = 'tvg-id=';
  static const ICON_TAG = 'tvg-logo=';
  static const GROUP_TAG = 'group-title=';

  // public
  Future<List<M3uEntry>?> parseFromString() async {
    final streams = _splitM3uEntry(file);
    if (streams == null) {
      return null;
    }

    return streams;
  }

  _TagsM3U _splitTag(String _tags) {
    final _TagsM3U tags = _TagsM3U();

    void _addTag(String tag, String info) {
      if (tag.contains(ID_TAG)) {
        tags.id = info;
      } else if (tag.contains(NAME_TAG)) {
        tags.name = info;
      } else if (tag.contains(GROUP_TAG)) {
        tags.group = [info];
      } else if (tag.contains(ICON_TAG)) {
        tags.icon = info;
      }
    }

    final _temp = _tags.split('"');
    for (int i = 0; i < _temp.length; i++) {
      if (i + 1 >= _temp.length) {
        break;
      }
      _addTag(_temp[i], _temp[i + 1]);
      i++;
    }
    return tags;
  }

  Map<String, dynamic> _splitM3U(String channel) {
    final List<String> _infoAndLink = channel.split('\n');
    final String _info = _infoAndLink[0];
    final List<String> _tagAndName = _info.split(',');
    final _TagsM3U _tagList = _splitTag(_info.substring(0, _info.length - _tagAndName.last.length));
    final Map<String, String?> _attributes = {
      ID_TAG: _tagList.id,
      ICON_TAG: _tagList.icon,
      GROUP_TAG: _tagList.group?.first
    };

    return {
      ID_FIELD: _tagList.id,
      ICON_FIELD: _tagList.icon,
      GROUP_FIELD: _tagList.group,
      NAME_FIELD: _tagAndName.last,
      PRIMARY_URL_FIELD: _infoAndLink[1],
      ATTRIBUTES_FIELD: _attributes
    };
  }

  List<M3uEntry>? _splitM3uEntry(String streamsString) {
    final List<M3uEntry> streams = [];

    List<String> result = [];

    final divider = _findDivider(streamsString);
    if (divider == null) {
      return null;
    }

    try {
      result = streamsString.split(divider);
    } catch (e) {
      return null;
    }
    for (int i = 1; i < result.length; i++) {
      final _m3u = _splitM3U(result[i]);
      String name = _m3u[NAME_FIELD];
      if (name.endsWith('\r')) {
        name = name.substring(0, name.length - 1);
      }
      final attr = _m3u[ATTRIBUTES_FIELD] ?? {'': ''};
      String link = _m3u[PRIMARY_URL_FIELD];
      if (link.endsWith('\r')) {
        link = link.substring(0, link.length - 1);
      }
      final entry = M3uEntry(title: name, attributes: attr, link: link);
      streams.add(entry);
    }
    return streams;
  }

  String? _findDivider(String m3u) {
    final reg = RegExp('#EXTINF:-1');
    final _match = reg.firstMatch(m3u)?.group(0);
    try {
      return _match?.split('tvg')[0];
    } catch (e) {
      return null;
    }
  }
}

class _TagsM3U {
  String? id;
  String? name;
  String? icon;
  List<String>? group;

  _TagsM3U({this.group, this.icon, this.id, this.name});
}
