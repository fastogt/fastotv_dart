import 'package:fastotv_dart/commands_info.dart';
import 'package:objectid/objectid.dart';

String _uniqueID() {
  return ObjectId().hexString;
}

class M3uEntry {
  M3uEntry({required this.title, required this.attributes, required this.link, this.duration});

  String title;
  Map<String, String?> attributes;
  String link;
  int? duration;
}

typedef OnParseCallback = Function(int cur, int total);

class ChannelsFromFile {
  final List<M3uEntry> playlist;

  ChannelsFromFile(this.playlist);

  final List<ChannelInfo> channels = [];
  final List<VodInfo> vods = [];
  final List<VodInfo> episodes = [];
  final List<SeasonInfo> seasons = [];
  final List<SerialInfo> series = [];

  final validSerialDot = RegExp(r"[.\s]*S(\d+)[.\s]*E(\d+)");
  final stableTitle = RegExp(r"(.*) S(\d+) (.*)");
  static const NAME_TAG = 'tvg-name=';
  static const ID_TAG = 'tvg-id=';
  static const ICON_TAG = 'tvg-logo=';
  static const GROUP_TAG = 'group-title=';

  PackageInfo makePackage(String name, String description, {OnParseCallback? callback}) {
    channels.clear();
    vods.clear();
    episodes.clear();
    seasons.clear();
    series.clear();

    for (int i = 0; i < playlist.length; ++i) {
      callback?.call(i, playlist.length);
      final el = playlist[i];
      final splited = el.link.split('.');
      final String ext = splited.last.toLowerCase();
      if (ext == 'mp4' || ext == 'mkv' || ext == 'ts' || ext == 'avi') {
        // حريم الشاويش S01 Hreem-Alshaweesh.S1.E1
        // نحب تاني ليه S01 wenheb,tany,leh.S01.E01
        // Peaky Blinders S03 Peaky Blinders.S03.E06
        // Peaky Blinders – Gangs of Birmingham S01 Peaky blinders.S01.E01
        final tvgName = el.attributes[NAME_TAG];
        if (tvgName != null && tvgName != el.title) {
          el.title = tvgName;
        }
        final List<String> groups = [];
        if (el.attributes.containsKey(GROUP_TAG)) {
          final maybeGroups = el.attributes[GROUP_TAG];
          if (maybeGroups != null) {
            groups.add(maybeGroups);
          }
        }
        final m = validSerialDot.firstMatch(el.title);
        if (m != null) {
          final full = m[0]!;
          final longTitle = el.title.substring(0, el.title.length - full.length);
          String serialTitle = longTitle;
          final m2 = stableTitle.firstMatch(serialTitle);
          if (m2 != null) {
            serialTitle = m2[1]!;
          }

          final seasonNumber = m[1]!;
          final episodeNumber = m[2]!;

          final eid = '$longTitle+E$episodeNumber';
          final epi = _createEpisode(el, eid);
          episodes.add(epi);

          final ind = int.parse(seasonNumber);
          String sid = '$longTitle+S$ind';
          if (groups.isNotEmpty) {
            final gr = groups[0];
            sid += '+G$gr';
            serialTitle += ' ($gr)';
          }
          final seasonName = 'S$seasonNumber $serialTitle';
          final seasonIndex = seasons.indexWhere((element) => element.id == sid);
          SeasonInfo cur;
          if (seasonIndex != -1) {
            cur = seasons[seasonIndex];
            cur.episodes.add(eid);
          } else {
            cur = _createSeason(seasonName, sid, epi.vod.previewIcon, ind, [eid]);
            seasons.add(cur);
          }

          final serialIndex = series.indexWhere((element) => element.name == serialTitle);
          if (serialIndex == -1) {
            final ser = _createSerial(serialTitle, epi.vod.previewIcon, [cur.id], groups);
            series.add(ser);
          } else {
            if (!series[serialIndex].seasons.contains(cur.id)) {
              series[serialIndex].seasons.add(cur.id);
            }
          }
          continue;
        }

        vods.add(_createVod(el));
      } else {
        channels.add(_createLiveStream(el));
      }
    }
    return _makePackage(name, description);
  }

  ChannelInfo _createLiveStream(M3uEntry element) {
    String eid = '';
    if (element.attributes.containsKey(ID_TAG)) {
      final maybeId = element.attributes[ID_TAG];
      if (maybeId != null) {
        eid = maybeId;
      }
    }
    final icon = element.attributes[ICON_TAG] ??
        'https://api.fastocloud.com/install/assets/unknown_channel.png';
    const description = '';
    final PlayingUrl play = PlayingUrl(url: element.link);
    final _epg = EpgInfo(
        id: eid, urls: [play], displayName: element.title, icon: icon, description: description);
    final List<String> tags = [];
    if (element.attributes.containsKey(GROUP_TAG)) {
      final maybeGroups = element.attributes[GROUP_TAG];
      if (maybeGroups != null) {
        tags.add(maybeGroups);
      }
    }
    final String sid = _uniqueID();
    final _channelInfo = ChannelInfo(
        id: sid,
        groups: tags,
        iarc: 21,
        favorite: false,
        recent: 0,
        interruptTime: 0,
        epg: _epg,
        parts: [],
        meta: <MetaUrl>[],
        subtitles: <Subtitle>[],
        createdDate: 0);

    return _channelInfo;
  }

  VodInfo _createVod(M3uEntry element) {
    final icon = element.attributes[ICON_TAG] ??
        'https://api.fastocloud.com/install/assets/unknown_channel.png';
    final PlayingUrl play = PlayingUrl(url: element.link);
    final _movieInfo = MovieInfo(
        urls: [play],
        description: '',
        displayName: element.title,
        backgroundIcon: icon,
        previewIcon: icon,
        trailerUrl: '',
        userScore: 0.0,
        primeDate: 0,
        country: '',
        duration: 0,
        directors: [],
        cast: [],
        production: [],
        genres: []);
    final List<String> groups = [];
    if (element.attributes.containsKey(GROUP_TAG)) {
      final maybeGroups = element.attributes[GROUP_TAG];
      if (maybeGroups != null) {
        groups.add(maybeGroups);
      }
    }

    final String sid = _uniqueID();
    final vodInfo = VodInfo(
        id: sid,
        groups: groups,
        iarc: 21,
        favorite: false,
        recent: 0,
        interruptTime: 0,
        vod: _movieInfo,
        parts: [],
        meta: <MetaUrl>[],
        subtitles: <Subtitle>[],
        createdDate: 0);
    return vodInfo;
  }

  VodInfo _createEpisode(M3uEntry element, String eid) {
    final icon = element.attributes[ICON_TAG] ??
        'https://api.fastocloud.com/install/assets/unknown_channel.png';
    final PlayingUrl play = PlayingUrl(url: element.link);
    final _movieInfo = MovieInfo(
        urls: [play],
        description: '',
        displayName: element.title,
        backgroundIcon: icon,
        previewIcon: icon,
        trailerUrl: '',
        userScore: 0.0,
        primeDate: 0,
        country: '',
        duration: 0,
        directors: [],
        cast: [],
        production: [],
        genres: []);
    final List<String> groups = [];
    if (element.attributes.containsKey(GROUP_TAG)) {
      final maybeGroups = element.attributes[GROUP_TAG];
      if (maybeGroups != null) {
        groups.add(maybeGroups);
      }
    }

    final vodInfo = VodInfo(
        id: eid,
        groups: groups,
        iarc: 21,
        favorite: false,
        recent: 0,
        interruptTime: 0,
        vod: _movieInfo,
        parts: [],
        meta: <MetaUrl>[],
        subtitles: <Subtitle>[],
        createdDate: 0);
    return vodInfo;
  }

  SerialInfo _createSerial(String title, String icon, List<String> seasons, List<String> groups) {
    const back = 'https://api.fastocloud.com/install/assets/unknown_background.png';
    final String sid = _uniqueID();
    return SerialInfo(
        favorite: false,
        recent: 0,
        id: sid,
        name: title,
        background: back,
        previewIcon: icon,
        groups: groups,
        iarc: 21,
        description: '',
        seasons: seasons,
        primeDate: 0,
        createdDate: 0,
        userScore: 0.0,
        country: '',
        directors: [],
        cast: [],
        production: [],
        genres: []);
  }

  SeasonInfo _createSeason(
      String title, String sid, String icon, int number, List<String> episodes) {
    const back = 'https://api.fastocloud.com/install/assets/unknown_background.png';
    return SeasonInfo(
        id: sid,
        name: title,
        background: back,
        previewIcon: icon,
        description: '',
        season: number,
        episodes: episodes,
        createdDate: 0);
  }

  PackageInfo _makePackage(String name, String description) {
    const back = 'https://api.fastocloud.com/install/assets/unknown_background.png';
    return PackageInfo(
        name: name,
        description: description,
        backgroundUrl: back,
        streams: channels,
        catchups: [],
        vods: vods,
        series: episodes,
        seasons: seasons,
        serials: series);
  }
}
