import 'package:fastotv_dart/commands_info/playing_url.dart';

class EpgInfo {
  static const _ID_FIELD = 'id';

  static const _URLS_FIELD = 'urls';
  static const _DISPLAY_NAME_FIELD = 'display_name';
  static const _ICON_FIELD = 'icon';
  static const _DESCRIPTION_FIELD = 'description';

  String id;
  List<PlayingUrl> urls;
  String displayName;
  String icon;
  String description;

  EpgInfo(
      {required this.id,
      required this.urls,
      required this.displayName,
      required this.icon,
      required this.description});

  factory EpgInfo.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final raw = json[_URLS_FIELD];
    final List<PlayingUrl> urls = [];
    for (final element in raw) {
      final lc = PlayingUrl.fromJson(element);
      urls.add(lc);
    }
    final displayName = json[_DISPLAY_NAME_FIELD];
    final icon = json[_ICON_FIELD];
    final description = json[_DESCRIPTION_FIELD];
    return EpgInfo(
        id: id, urls: urls, displayName: displayName, icon: icon, description: description);
  }

  EpgInfo copy() {
    return EpgInfo(
        id: id, urls: urls, displayName: displayName, icon: icon, description: description);
  }

  Map<String, dynamic> toJson() {
    return {_ID_FIELD: id, _URLS_FIELD: urls, _DISPLAY_NAME_FIELD: displayName, _ICON_FIELD: icon};
  }
}
