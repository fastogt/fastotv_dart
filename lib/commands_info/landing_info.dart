import 'package:fastotv_dart/commands_info/platform.dart';

class LandingPlatformInfo {
  LandingPlatformInfo({required this.os, required this.url});

  LandingPlatformInfo copy() {
    return LandingPlatformInfo(os: os, url: url);
  }

  PlatformType os;
  String url;

  static const String URL_FIELD = 'url';
  static const String OS_TYPE_FIELD = 'os';

  static LandingPlatformInfo web = LandingPlatformInfo(os: PlatformType.WEB, url: '');
  static LandingPlatformInfo android = LandingPlatformInfo(os: PlatformType.ANDROID, url: '');
  static LandingPlatformInfo ios = LandingPlatformInfo(os: PlatformType.IOS, url: '');
  static LandingPlatformInfo windows = LandingPlatformInfo(os: PlatformType.WINDOWS, url: '');
  static LandingPlatformInfo linux = LandingPlatformInfo(os: PlatformType.LINUX, url: '');
  static LandingPlatformInfo macosx = LandingPlatformInfo(os: PlatformType.MACOSX, url: '');
  static LandingPlatformInfo appleTV = LandingPlatformInfo(os: PlatformType.APPLE_TV, url: '');
  static LandingPlatformInfo roku = LandingPlatformInfo(os: PlatformType.ROKU, url: '');
  static LandingPlatformInfo webos = LandingPlatformInfo(os: PlatformType.WEBOS, url: '');
  static LandingPlatformInfo tizen = LandingPlatformInfo(os: PlatformType.TIZEN, url: '');
  static LandingPlatformInfo unknown = LandingPlatformInfo(os: PlatformType.UNKNOWN, url: '');

  static List<LandingPlatformInfo> get values =>
      [web, android, ios, windows, linux, macosx, appleTV, roku, webos, tizen, unknown];

  factory LandingPlatformInfo.fromJson(Map<String, dynamic> json) {
    final type = json[OS_TYPE_FIELD];
    final url = json[URL_FIELD];
    return LandingPlatformInfo(os: PlatformType.fromString(type), url: url);
  }

  Map<String, dynamic> toJson() {
    return {OS_TYPE_FIELD: os.os, URL_FIELD: url};
  }
}
