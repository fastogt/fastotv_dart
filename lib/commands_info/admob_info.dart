import 'package:fastotv_dart/commands_info/iadservice_info.dart';

class AdmobUnit {
  static const _APP_ID_FIELD = 'app_id';
  static const _BANNER_FIELD = 'banner';
  static const _INTERSTITIAL_FIELD = 'interstitial';
  static const _REWARDED_FIELD = 'rewarded';

  String appid;
  String? banner;
  String? interstitial;
  String? rewarded;

  AdmobUnit({required this.appid, this.banner, this.interstitial, this.rewarded});

  factory AdmobUnit.createDefault() {
    return AdmobUnit(appid: 'SOME_ID');
  }

  AdmobUnit copy() {
    return AdmobUnit(appid: appid, banner: banner, interstitial: interstitial, rewarded: rewarded);
  }

  factory AdmobUnit.fromJson(Map<String, dynamic> json) {
    String? banner;
    if (json.containsKey(_BANNER_FIELD)) {
      banner = json[_BANNER_FIELD];
    }
    String? interstitial;
    if (json.containsKey(_INTERSTITIAL_FIELD)) {
      interstitial = json[_INTERSTITIAL_FIELD];
    }
    String? rewarded;
    if (json.containsKey(_REWARDED_FIELD)) {
      rewarded = json[_REWARDED_FIELD];
    }
    return AdmobUnit(
        appid: json[_APP_ID_FIELD], banner: banner, interstitial: interstitial, rewarded: rewarded);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_APP_ID_FIELD: appid};
    if (banner != null) {
      result[_BANNER_FIELD] = banner;
    }
    if (interstitial != null) {
      result[_INTERSTITIAL_FIELD] = interstitial;
    }
    if (rewarded != null) {
      result[_REWARDED_FIELD] = rewarded;
    }
    return result;
  }
}

class GoogleAdMob extends IAdService {
  static const GOOGLE_ADMOB_FIELD = 'google_admob';

  GoogleAdMobData googleAdmob;

  GoogleAdMob({required this.googleAdmob}) : super(type: AdType.ADMOB);

  GoogleAdMob.createDefault()
      : googleAdmob = GoogleAdMobData.createDefault(),
        super(type: AdType.ADMOB);

  @override
  GoogleAdMob copy() {
    return GoogleAdMob(googleAdmob: googleAdmob.copy());
  }

  factory GoogleAdMob.fromJson(Map<String, dynamic> json) {
    return GoogleAdMob(googleAdmob: GoogleAdMobData.fromJson(json[GOOGLE_ADMOB_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[GOOGLE_ADMOB_FIELD] = googleAdmob.toJson();
    return result;
  }
}

class GoogleAdMobData {
  static const _ANDROID_FIELD = 'android';
  static const _IOS_FIELD = 'ios';

  AdmobUnit? android;
  AdmobUnit? ios;

  GoogleAdMobData({this.android, this.ios});

  factory GoogleAdMobData.createDefault() {
    return GoogleAdMobData();
  }

  GoogleAdMobData copy() {
    return GoogleAdMobData(android: android, ios: ios);
  }

  factory GoogleAdMobData.fromJson(Map<String, dynamic> json) {
    AdmobUnit? android;
    if (json.containsKey(_ANDROID_FIELD)) {
      android = AdmobUnit.fromJson(json[_ANDROID_FIELD]);
    }
    AdmobUnit? ios;
    if (json.containsKey(_IOS_FIELD)) {
      ios = AdmobUnit.fromJson(json[_IOS_FIELD]);
    }
    return GoogleAdMobData(android: android, ios: ios);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    if (android != null) {
      result[_ANDROID_FIELD] = android;
    }
    if (ios != null) {
      result[_IOS_FIELD] = ios;
    }
    return result;
  }
}
