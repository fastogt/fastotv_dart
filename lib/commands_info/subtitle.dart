class Subtitle {
  static const _LANGUAGE_FIELD = 'language';
  static const _URL_FIELD = 'url';

  String language;
  String url;

  Subtitle({required this.language, required this.url});

  Subtitle copy() {
    return Subtitle(language: language, url: url);
  }

  factory Subtitle.fromJson(Map<String, dynamic> json) {
    final lang = json[_LANGUAGE_FIELD];
    final url = json[_URL_FIELD];
    return Subtitle(language: lang, url: url);
  }

  Map<String, dynamic> toJson() {
    return {_LANGUAGE_FIELD: language, _URL_FIELD: url};
  }
}
