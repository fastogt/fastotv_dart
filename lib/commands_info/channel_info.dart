import 'package:fastotv_dart/commands_info/catchup_info.dart';
import 'package:fastotv_dart/commands_info/epg_info.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/playing_url.dart';
import 'package:fastotv_dart/commands_info/stream_base_info.dart';
import 'package:fastotv_dart/commands_info/subtitle.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/utils.dart';

class ChannelInfo extends StreamBaseInfo implements IDisplayContentInfo {
  static const EPG_FIELD = 'epg';

  final EpgInfo epg;

  ChannelInfo(
      {required String id,
      required List<String> groups,
      required int iarc,
      required bool favorite,
      required int recent,
      required int interruptTime,
      required List<String> parts,
      required List<MetaUrl> meta,
      required List<Subtitle> subtitles,
      required UtcTimeMsec createdDate,
      PricePack? price,
      String? pid,
      required this.epg})
      : super(
            id: id,
            groups: groups,
            iarc: iarc,
            favorite: favorite,
            recent: recent,
            interruptTime: interruptTime,
            parts: parts,
            meta: meta,
            subtitles: subtitles,
            createdDate: createdDate,
            price: price,
            pid: pid);

  @override
  ChannelInfo copy() {
    return ChannelInfo(
        id: id,
        groups: groups,
        iarc: iarc,
        favorite: favorite(),
        recent: recentTime(),
        interruptTime: interruptTime(),
        parts: parts,
        meta: meta,
        subtitles: subtitles,
        createdDate: createdDate,
        price: price,
        pid: pid,
        epg: epg);
  }

  @override
  String displayName() {
    return epg.displayName;
  }

  @override
  String icon() {
    return epg.icon;
  }

  List<PlayingUrl> urls() {
    return epg.urls;
  }

  factory ChannelInfo.fromJson(Map<String, dynamic> json) {
    final base = StreamBaseInfo.fromJson(json);
    final epg = EpgInfo.fromJson(json[EPG_FIELD]);

    return ChannelInfo(
        id: base.id,
        groups: base.groups,
        iarc: base.iarc,
        favorite: base.favorite(),
        recent: base.recentTime(),
        interruptTime: base.interruptTime(),
        epg: epg,
        parts: base.parts,
        meta: base.meta,
        subtitles: base.subtitles,
        createdDate: base.createdDate,
        price: base.price,
        pid: base.pid);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> base = super.toJson();
    base[EPG_FIELD] = epg.toJson();
    return base;
  }

  List<CatchupInfo> catchupsInfos(List<CatchupInfo> cache) {
    final List<CatchupInfo> _catchups = [];
    for (final cid in parts) {
      for (final catchup in cache) {
        if (cid == catchup.id) {
          _catchups.add(catchup);
          break;
        }
      }
    }
    return _catchups;
  }

  CatchupInfo? findCatchupByTime(int start, int stop, List<CatchupInfo> cache) {
    for (final cid in parts) {
      for (final catchup in cache) {
        if (cid == catchup.id && start >= catchup.start && stop <= catchup.stop) {
          return catchup;
        }
      }
    }
    return null;
  }
}
