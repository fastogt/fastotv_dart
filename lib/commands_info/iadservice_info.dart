import 'package:fastotv_dart/commands_info/admob_info.dart';
import 'package:fastotv_dart/commands_info/promotion_info.dart';
import 'package:fastotv_dart/commands_info/vast_info.dart';

class AdType {
  const AdType._(this._value);

  static const int _PROMO = 0;
  static const int _ADMOB = 1;
  static const int _VAST = 2;

  static const AdType PROMO = AdType._(_PROMO);
  static const AdType ADMOB = AdType._(_ADMOB);
  static const AdType VAST = AdType._(_VAST);

  final int _value;

  static List<AdType> get values => [PROMO, ADMOB, VAST];

  int compareTo(AdType status) {
    return _value.compareTo(status._value);
  }

  String toHumanReadable() {
    if (_value == _PROMO) {
      return 'PROMO';
    } else if (_value == _ADMOB) {
      return 'ADMOB';
    } else if (_value == _VAST) {
      return 'VAST';
    }
    return 'PROMO';
  }

  factory AdType.fromInt(int mode) {
    if (mode == _PROMO) {
      return AdType.PROMO;
    } else if (mode == _ADMOB) {
      return AdType.ADMOB;
    } else if (mode == _VAST) {
      return AdType.VAST;
    }

    throw 'Unknown ad type: $mode';
  }

  int toInt() {
    return _value;
  }
}

abstract class IAdService {
  static const TYPE_FIELD = 'type';

  final AdType type;

  IAdService({required this.type});

  IAdService copy();

  factory IAdService.fromJson(Map<String, dynamic> json) {
    final type = AdType.fromInt(json[TYPE_FIELD]);

    if (type == AdType.PROMO) {
      return Promotion.fromJson(json);
    } else if (type == AdType.ADMOB) {
      return GoogleAdMob.fromJson(json);
    } else if (type == AdType.VAST) {
      return Vast.fromJson(json);
    }

    throw 'Unknown payment type: $type';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {TYPE_FIELD: type.toInt()};
    return result;
  }
}
