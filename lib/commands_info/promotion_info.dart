import 'iadservice_info.dart';

class PromotionType {
  const PromotionType._(this._value);

  static const int _TEXT_NUM = 0;
  static const int _LINK_NUM = 1;
  static const int _IMAGE_NUM = 2;
  static const int _VIDEO_NUM = 3;

  static const String TEXT_PROMO = 'Text';
  static const String LINK_PROMO = 'Link';
  static const String IMAGE_PROMO = 'Image';
  static const String VIDEO_PROMO = 'Video';

  static const PromotionType TEXT = PromotionType._(_TEXT_NUM);
  static const PromotionType LINK = PromotionType._(_LINK_NUM);
  static const PromotionType IMAGE = PromotionType._(_IMAGE_NUM);
  static const PromotionType VIDEO = PromotionType._(_VIDEO_NUM);

  final int _value;

  static List<PromotionType> get values => [TEXT, LINK, IMAGE, VIDEO];

  static List<String> get stringValues => [TEXT_PROMO, LINK_PROMO, IMAGE_PROMO, VIDEO_PROMO];

  int compareTo(PromotionType status) {
    return _value.compareTo(status._value);
  }

  String toHumanReadable() {
    if (_value == _TEXT_NUM) {
      return TEXT_PROMO;
    } else if (_value == _LINK_NUM) {
      return LINK_PROMO;
    } else if (_value == _IMAGE_NUM) {
      return IMAGE_PROMO;
    } else if (_value == _VIDEO_NUM) {
      return VIDEO_PROMO;
    }
    return TEXT_PROMO;
  }

  factory PromotionType.fromInt(int type) {
    if (type == _TEXT_NUM) {
      return PromotionType.TEXT;
    } else if (type == _LINK_NUM) {
      return PromotionType.LINK;
    } else if (type == _IMAGE_NUM) {
      return PromotionType.IMAGE;
    } else if (type == _VIDEO_NUM) {
      return PromotionType.VIDEO;
    }

    throw 'Unknown promo type: $type';
  }

  int toInt() {
    return _value;
  }
}

class Promotion extends IAdService {
  static const PROMOTION_FIELD = 'promotion';

  PromotionData promotion;

  Promotion({required this.promotion}) : super(type: AdType.PROMO);

  Promotion.createDefault()
      : promotion = PromotionData.createDefault(),
        super(type: AdType.PROMO);

  @override
  Promotion copy() {
    return Promotion(promotion: promotion.copy());
  }

  factory Promotion.fromJson(Map<String, dynamic> json) {
    return Promotion(promotion: PromotionData.fromJson(json[PROMOTION_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[PROMOTION_FIELD] = promotion.toJson();
    return result;
  }
}

class PromotionData {
  static const _URL_FIELD = 'url';
  static const _CUSTOM_TYPE_FIELD = 'type';

  String url;
  PromotionType customType;

  PromotionData({required this.url, required this.customType});

  factory PromotionData.createDefault() {
    return PromotionData(url: 'https://crocott.com', customType: PromotionType.LINK);
  }

  PromotionData copy() {
    return PromotionData(url: url, customType: customType);
  }

  factory PromotionData.fromJson(Map<String, dynamic> json) {
    final type = PromotionType.fromInt(json[_CUSTOM_TYPE_FIELD]);
    return PromotionData(url: json[_URL_FIELD], customType: type);
  }

  Map<String, dynamic> toJson() {
    final result = {_URL_FIELD: url, _CUSTOM_TYPE_FIELD: customType.toInt()};
    return result;
  }
}
