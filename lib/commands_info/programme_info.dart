String _twoDigits(int n) {
  if (n >= 10) {
    return "$n";
  }
  return "0$n";
}

class ProgrammeInfo {
  static const _CHANNEL_FIELD = 'channel';
  static const _START_FIELD = 'start';
  static const _STOP_FIELD = 'stop';
  static const _TITLE_FIELD = 'title';
  static const _CATEGORY_FIELD = 'category';
  static const _DESCRIPTION_FIELD = 'desc';
  static const _ICON_FIELD = 'icon';

  final String channel;
  final int start;
  final int stop;
  final String title;
  final String? category;
  final String? description;
  final String? icon;

  ProgrammeInfo(
      this.channel, this.start, this.stop, this.title, this.category, this.description, this.icon);

  ProgrammeInfo copy() {
    return ProgrammeInfo(channel, start, stop, title, category, description, icon);
  }

  factory ProgrammeInfo.fromJson(Map<String, dynamic> json) {
    final channel = json[_CHANNEL_FIELD];
    final start = json[_START_FIELD];
    final stop = json[_STOP_FIELD];
    final title = json[_TITLE_FIELD];
    // optional
    final category = json[_CATEGORY_FIELD];
    final desc = json[_DESCRIPTION_FIELD];
    final icon = json[_ICON_FIELD];
    return ProgrammeInfo(channel, start, stop, title, category, desc, icon);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      _CHANNEL_FIELD: channel,
      _START_FIELD: start,
      _STOP_FIELD: stop,
      _TITLE_FIELD: title
    };
    if (category != null) {
      result[_CATEGORY_FIELD] = category;
    }
    if (description != null) {
      result[_DESCRIPTION_FIELD] = description;
    }
    if (icon != null) {
      result[_ICON_FIELD] = icon;
    }
    return result;
  }

  String startText(Duration timeZoneOffset) {
    final startTime = Duration(milliseconds: start) + timeZoneOffset;
    final diff = startTime - Duration(days: startTime.inDays);

    final hours = diff.inHours;
    final minutes = (diff - Duration(hours: diff.inHours)).inMinutes;
    return "${_twoDigits(hours)}:${_twoDigits(minutes)}";
  }

  String endText(Duration timeZoneOffset) {
    final Duration stopTime = Duration(milliseconds: stop) + timeZoneOffset;
    final Duration diff = stopTime - Duration(days: stopTime.inDays);

    final int hours = diff.inHours;
    final int minutes = (diff - Duration(hours: diff.inHours)).inMinutes;
    return "${_twoDigits(hours)}:${_twoDigits(minutes)}";
  }

  int get durationMsec {
    return stop - start;
  }

  String durationText() {
    final Duration diff = Duration(milliseconds: durationMsec);
    final String twoDigitMinutes = _twoDigits(diff.inMinutes.remainder(60));
    return '${_twoDigits(diff.inHours)}:$twoDigitMinutes';
  }
}
