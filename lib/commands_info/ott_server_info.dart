import 'package:fastotv_dart/commands_info/iadservice_info.dart';
import 'package:fastotv_dart/commands_info/landing_info.dart';
import 'package:fastotv_dart/commands_info/public_package.dart';
import 'package:fastotv_dart/commands_info/types.dart';

class OttSettings {
  static const _LOGO_FIELD = 'logo';
  static const _TITLE_FIELD = 'title';
  static const _LANDING_FIELD = 'landing';
  static const _SIGNIN_FIELD = 'signin';
  static const _SIGNUP_FIELD = 'signup';
  static const _INTRO_FIELD = 'intro';
  static const _ORGANIZATION_FIELD = 'organization';
  static const _CONTACT_FIELD = 'contact';
  static const _PLATFORMS_FIELD = 'platforms';
  static const _THEME_FIELD = 'theme';
  static const _MODE_FIELD = 'mode';
  static const _PROMOTION_FIELD = 'ad_service';

  String logo;
  String title;
  String landing;
  String signin;
  String signup;
  String intro;
  String organization;
  String contact;
  List<LandingPlatformInfo> platforms;
  GlobalTheme theme;
  WsMode mode;
  IAdService? promo;

  String get backgroundUrl => logo;

  OttSettings(
      {required this.logo,
      required this.title,
      required this.landing,
      required this.signin,
      required this.signup,
      required this.intro,
      required this.organization,
      required this.contact,
      required this.platforms,
      required this.theme,
      required this.mode,
      this.promo});

  OttSettings copy() {
    return OttSettings(
        logo: logo,
        title: title,
        landing: landing,
        signin: signin,
        signup: signup,
        intro: intro,
        organization: organization,
        contact: contact,
        platforms: platforms,
        theme: theme,
        mode: mode,
        promo: promo);
  }

  factory OttSettings.fromJson(Map<String, dynamic> json) {
    final List<LandingPlatformInfo> platforms = [];
    final platformsRawData = json[_PLATFORMS_FIELD];
    platformsRawData?.forEach((item) {
      final platform = LandingPlatformInfo.fromJson(item);
      platforms.add(platform);
    });
    IAdService? promo;
    if (json.containsKey(_PROMOTION_FIELD)) {
      promo = IAdService.fromJson(json[_PROMOTION_FIELD]);
    }
    return OttSettings(
        logo: json[_LOGO_FIELD],
        title: json[_TITLE_FIELD],
        landing: json[_LANDING_FIELD],
        signin: json[_SIGNIN_FIELD],
        signup: json[_SIGNUP_FIELD],
        intro: json[_INTRO_FIELD],
        organization: json[_ORGANIZATION_FIELD],
        contact: json[_CONTACT_FIELD],
        platforms: platforms,
        theme: GlobalTheme.fromInt(json[_THEME_FIELD]),
        mode: WsMode.fromInt(json[_MODE_FIELD]),
        promo: promo);
  }

  Map<String, dynamic> toJson() {
    final platformsJson = [];
    for (final item in platforms) {
      platformsJson.add(item.toJson());
    }
    final result = {
      _TITLE_FIELD: title,
      _LOGO_FIELD: logo,
      _LANDING_FIELD: landing,
      _SIGNIN_FIELD: signin,
      _SIGNUP_FIELD: signup,
      _INTRO_FIELD: intro,
      _ORGANIZATION_FIELD: organization,
      _CONTACT_FIELD: contact,
      _PLATFORMS_FIELD: platformsJson,
      _THEME_FIELD: theme.toInt(),
      _MODE_FIELD: mode.toInt()
    };
    if (promo != null) {
      result[_PROMOTION_FIELD] = promo!.toJson();
    }
    return result;
  }
}

class GlobalTheme {
  const GlobalTheme._(this._value);

  static const int _LIGHT_THEME_NUM = 0;
  static const int _DARK_THEME_NUM = 1;

  static const String LIGHT_THEME = 'Light';
  static const String DARK_THEME = 'Dark';

  static const GlobalTheme LIGHT = GlobalTheme._(_LIGHT_THEME_NUM);
  static const GlobalTheme DARK = GlobalTheme._(_DARK_THEME_NUM);

  int compareTo(GlobalTheme status) {
    return _value.compareTo(status._value);
  }

  final int _value;

  static List<GlobalTheme> get values => [LIGHT, DARK];

  static List<String> get stringValues => [LIGHT_THEME, DARK_THEME];

  String toHumanReadable() {
    if (_value == _LIGHT_THEME_NUM) {
      return LIGHT_THEME;
    } else if (_value == _DARK_THEME_NUM) {
      return DARK_THEME;
    }
    return LIGHT_THEME;
  }

  factory GlobalTheme.fromInt(int type) {
    if (type == _LIGHT_THEME_NUM) {
      return GlobalTheme.LIGHT;
    } else if (type == _DARK_THEME_NUM) {
      return GlobalTheme.DARK;
    }

    throw 'Unknown theme type: $type';
  }

  int toInt() {
    return _value;
  }
}

class OttServerInfo {
  static const _PROVIDERS_COUNT_FIELD = 'providers_count';
  static const _BRAND_FIELD = 'brand';
  static const _PACKAGES_FIELD = 'packages';

  int providersCount;
  OttSettings brand;
  List<PackagePublic> packages;

  OttServerInfo({required this.providersCount, required this.brand, required this.packages});

  OttServerInfo copy() {
    return OttServerInfo(providersCount: providersCount, brand: brand.copy(), packages: packages);
  }

  factory OttServerInfo.fromJson(Map<String, dynamic> json) {
    final List<PackagePublic> packages = [];
    for (final element in json[_PACKAGES_FIELD]) {
      final pack = PackagePublic.fromJson(element);
      packages.add(pack);
    }
    return OttServerInfo(
        providersCount: json[_PROVIDERS_COUNT_FIELD],
        brand: OttSettings.fromJson(json[_BRAND_FIELD]),
        packages: packages);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[_PROVIDERS_COUNT_FIELD] = providersCount;
    result[_BRAND_FIELD] = brand.toJson();
    result[_PACKAGES_FIELD] = packages;
    return result;
  }
}
