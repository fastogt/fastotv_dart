import 'package:fastotv_dart/commands_info/playing_url.dart';

class MovieInfo {
  static const _URLS_FIELD = 'urls';
  static const _DESCRIPTION_FIELD = 'description';
  static const _DISPLAY_NAME_FIELD = 'display_name';
  static const _PREVIEW_ICON_FIELD = 'icon';

  static const _BACKGROUND_FIELD = 'background_url';
  static const _TRAILER_URL_FIELD = 'trailer_url';
  static const _USER_SCORE_FIELD = 'user_score';
  static const _PRIME_DATE_FIELD = 'prime_date';
  static const _COUNTRY_FIELD = 'country';
  static const _DURATION_FIELD = 'duration';
  static const _DIRECTORS_FIELD = 'directors';
  static const _CAST_FIELD = 'cast';
  static const _PRODUCTION_FIELD = 'production';
  static const _GENRES_FIELD = 'genres';

  List<PlayingUrl> urls;
  String displayName;
  String previewIcon;
  String description;

  String backgroundIcon;
  String trailerUrl;
  double userScore;
  int primeDate;
  String country;
  int duration;
  List<String> directors; // ['Travolta', 'Mike']
  List<String> cast; // ['Jhonny Dep', 'Arnold']
  List<String> production; // ['WB', 'Netflix']
  List<String> genres; // ['Drama', 'Comedy']

  MovieInfo(
      {required this.urls,
      required this.description,
      required this.displayName,
      required this.previewIcon,
      required this.backgroundIcon,
      required this.trailerUrl,
      required this.userScore,
      required this.primeDate,
      required this.country,
      required this.duration,
      required this.directors,
      required this.cast,
      required this.production,
      required this.genres});

  factory MovieInfo.fromJson(Map<String, dynamic> json) {
    final raw = json[_URLS_FIELD];
    final List<PlayingUrl> urls = [];
    for (final element in raw) {
      final lc = PlayingUrl.fromJson(element);
      urls.add(lc);
    }
    final description = json[_DESCRIPTION_FIELD];
    final displayName = json[_DISPLAY_NAME_FIELD];
    final backgroundIcon = json[_BACKGROUND_FIELD];
    final previewIcon = json[_PREVIEW_ICON_FIELD];
    final trailerUrl = json[_TRAILER_URL_FIELD];
    final userScore = (json[_USER_SCORE_FIELD] as num).toDouble();
    final primeDate = json[_PRIME_DATE_FIELD];
    final country = json[_COUNTRY_FIELD];
    final duration = json[_DURATION_FIELD];
    final directors = json[_DIRECTORS_FIELD].cast<String>();
    final cast = json[_CAST_FIELD].cast<String>();
    final production = json[_PRODUCTION_FIELD].cast<String>();
    final genres = json[_GENRES_FIELD].cast<String>();
    return MovieInfo(
        urls: urls,
        description: description,
        displayName: displayName,
        backgroundIcon: backgroundIcon,
        previewIcon: previewIcon,
        trailerUrl: trailerUrl,
        userScore: userScore,
        primeDate: primeDate,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  MovieInfo copy() {
    return MovieInfo(
        urls: urls,
        description: description,
        displayName: displayName,
        backgroundIcon: backgroundIcon,
        previewIcon: previewIcon,
        trailerUrl: trailerUrl,
        userScore: userScore,
        primeDate: primeDate,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  Map<String, dynamic> toJson() {
    return {
      _URLS_FIELD: urls,
      _DESCRIPTION_FIELD: description,
      _DISPLAY_NAME_FIELD: displayName,
      _PREVIEW_ICON_FIELD: previewIcon,
      _BACKGROUND_FIELD: backgroundIcon,
      _TRAILER_URL_FIELD: trailerUrl,
      _USER_SCORE_FIELD: userScore,
      _PRIME_DATE_FIELD: primeDate,
      _COUNTRY_FIELD: country,
      _DURATION_FIELD: duration,
      _DIRECTORS_FIELD: directors,
      _CAST_FIELD: cast,
      _PRODUCTION_FIELD: production,
      _GENRES_FIELD: genres
    };
  }
}
