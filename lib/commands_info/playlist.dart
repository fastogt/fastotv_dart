class Playlist {
  static const _ID_FIELD = 'id';
  static const _NAME_FIELD = 'name';
  static const _URL_FIELD = 'url';
  static const _SELECT = 'select';

  String? id;
  String name;
  String url;
  bool select;

  Playlist({this.id, required this.name, required this.url, this.select = false});

  Playlist copy() {
    return Playlist(id: id, name: name, url: url, select: select);
  }

  factory Playlist.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final name = json[_NAME_FIELD];
    final url = json[_URL_FIELD];
    final select = json[_SELECT];

    return Playlist(id: id, name: name, url: url, select: select);
  }

  Map<String, dynamic> toJson() {
    return {_ID_FIELD: id, _NAME_FIELD: name, _URL_FIELD: url, _SELECT: select};
  }

  Map<String, dynamic> toSelectJson() {
    return {_ID_FIELD: id, _NAME_FIELD: name, _URL_FIELD: url, _SELECT: !select};
  }

  factory Playlist.createDefault() {
    return Playlist(name: 'playlist', url: 'https://exmaple.com/default.m3u');
  }

  bool isValid() {
    final isValidBase = Uri.parse(url).isAbsolute && name.isNotEmpty;
    if (id == null) {
      return isValidBase;
    }

    return isValidBase && id!.isNotEmpty;
  }
}
