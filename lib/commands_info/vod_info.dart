import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/movie_info.dart';
import 'package:fastotv_dart/commands_info/playing_url.dart';
import 'package:fastotv_dart/commands_info/stream_base_info.dart';
import 'package:fastotv_dart/commands_info/subtitle.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/utils.dart';

class VodInfo extends StreamBaseInfo implements IDisplayContentInfo {
  static const _VOD_FIELD = 'vod';

  final MovieInfo vod;

  VodInfo(
      {required String id,
      required List<String> groups,
      required int iarc,
      required bool favorite,
      required int recent,
      required int interruptTime,
      required List<String> parts,
      required List<MetaUrl> meta,
      required List<Subtitle> subtitles,
      required UtcTimeMsec createdDate,
      PricePack? price,
      String? pid,
      required this.vod})
      : super(
            id: id,
            groups: groups,
            iarc: iarc,
            favorite: favorite,
            recent: recent,
            interruptTime: interruptTime,
            parts: parts,
            meta: meta,
            subtitles: subtitles,
            createdDate: createdDate,
            price: price,
            pid: pid);

  List<PlayingUrl> urls() {
    return vod.urls;
  }

  @override
  String displayName() {
    return vod.displayName;
  }

  @override
  String icon() {
    return vod.previewIcon;
  }

  double userScore() {
    return vod.userScore;
  }

  int duration() {
    return vod.duration;
  }

  int primeDate() {
    return vod.primeDate;
  }

  String trailerUrl() {
    return vod.trailerUrl;
  }

  String country() {
    return vod.country;
  }

  @override
  VodInfo copy() {
    return VodInfo(
        id: id,
        groups: groups,
        iarc: iarc,
        favorite: favorite(),
        recent: recentTime(),
        interruptTime: interruptTime(),
        vod: vod.copy(),
        parts: parts,
        meta: meta,
        subtitles: subtitles,
        createdDate: createdDate,
        price: price,
        pid: pid);
  }

  factory VodInfo.fromJson(Map<String, dynamic> json) {
    final base = StreamBaseInfo.fromJson(json);
    final vod = MovieInfo.fromJson(json[_VOD_FIELD]);
    return VodInfo(
        id: base.id,
        groups: base.groups,
        iarc: base.iarc,
        favorite: base.favorite(),
        recent: base.recentTime(),
        interruptTime: base.interruptTime(),
        vod: vod,
        parts: base.parts,
        meta: base.meta,
        subtitles: base.subtitles,
        createdDate: base.createdDate,
        price: base.price,
        pid: base.pid);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> base = super.toJson();
    base[_VOD_FIELD] = vod.toJson();
    return base;
  }
}
