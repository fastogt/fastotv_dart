import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/subtitle.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/utils.dart';

class StreamBaseInfo {
  static const _ID_FIELD = 'id';
  static const _GROUPS_FIELD = 'groups';
  static const _IARC_FIELD = 'iarc';
  static const _PARTS_FIELD = 'parts';
  static const _META_FIELD = 'meta';
  static const _SUBTITLES_FIELD = 'subtitles';
  static const _CREATED_DATE_FIELD = 'created_date';

  // user
  static const _FAVORITE_FIELD = 'favorite';
  static const _RECENT_FIELD = 'recent';
  static const _INTERRUPT_TIME_FIELD = 'interrupt_time';

  // optional
  static const _PRICE_FIELD = 'price';
  static const _PID_FIELD = 'pid';

  String id;
  List<String> groups;
  int iarc;
  final List<String> parts;
  final List<MetaUrl> meta;
  final List<Subtitle> subtitles;
  final UtcTimeMsec createdDate;

  bool _favorite;
  int _recent;
  int _interruptTime;

  PricePack? price;
  String? pid;

  bool favorite() {
    return _favorite;
  }

  void setFavorite(bool value) {
    _favorite = value;
  }

  int interruptTime() {
    return _interruptTime;
  }

  void setInterruptTime(int value) {
    _interruptTime = value;
  }

  int recentTime() {
    return _recent;
  }

  void setRecentTime(int value) {
    _recent = value;
  }

  StreamBaseInfo(
      {required this.id,
      required this.groups,
      required this.iarc,
      required bool favorite,
      required int recent,
      required int interruptTime,
      required this.parts,
      required this.meta,
      required this.subtitles,
      required this.createdDate,
      this.price,
      this.pid})
      : _favorite = favorite,
        _interruptTime = interruptTime,
        _recent = recent;

  factory StreamBaseInfo.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final groups = json[_GROUPS_FIELD].cast<String>();
    final iarc = json[_IARC_FIELD];
    final favorite = json[_FAVORITE_FIELD];
    final recent = json[_RECENT_FIELD];
    final interruptTime = json[_INTERRUPT_TIME_FIELD];
    final parts = json[_PARTS_FIELD].cast<String>();
    final createdDate = json[_CREATED_DATE_FIELD];
    final List<MetaUrl> resultMeta = [];
    json[_META_FIELD].forEach((item) {
      resultMeta.add(MetaUrl.fromJson(item));
    });
    final List<Subtitle> resultSub = [];
    json[_SUBTITLES_FIELD].forEach((item) {
      resultSub.add(Subtitle.fromJson(item));
    });
    String? pid;
    if (json.containsKey(_PID_FIELD)) {
      pid = json[_PID_FIELD];
    }
    PricePack? price;
    if (json.containsKey(_PRICE_FIELD)) {
      price = PricePack.fromJson(json[_PRICE_FIELD]);
    }

    return StreamBaseInfo(
        id: id,
        groups: groups,
        iarc: iarc,
        favorite: favorite,
        recent: recent,
        interruptTime: interruptTime,
        parts: parts,
        meta: resultMeta,
        subtitles: resultSub,
        createdDate: createdDate,
        price: price,
        pid: pid);
  }

  StreamBaseInfo copy() {
    return StreamBaseInfo(
        id: id,
        groups: groups,
        iarc: iarc,
        favorite: _favorite,
        recent: _recent,
        interruptTime: _interruptTime,
        parts: parts,
        meta: meta,
        subtitles: subtitles,
        createdDate: createdDate,
        price: price,
        pid: pid);
  }

  Map<String, dynamic> toJson() {
    final result = {
      _ID_FIELD: id,
      _GROUPS_FIELD: groups,
      _IARC_FIELD: iarc,
      _FAVORITE_FIELD: favorite,
      _RECENT_FIELD: _recent,
      _INTERRUPT_TIME_FIELD: _interruptTime,
      _PARTS_FIELD: parts,
      _META_FIELD: meta,
      _SUBTITLES_FIELD: subtitles,
      _CREATED_DATE_FIELD: createdDate,
      _PRICE_FIELD: price
    };
    if (pid != null) {
      result[_PID_FIELD] = pid!;
    }
    if (price != null) {
      result[_PRICE_FIELD] = price!.toJson();
    }
    return result;
  }
}
