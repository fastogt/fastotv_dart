import 'package:fastotv_dart/commands_info/channel_info.dart';
import 'package:fastotv_dart/commands_info/epg_info.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/subtitle.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/utils.dart';

class CatchupInfo extends ChannelInfo {
  static const START_FIELD = 'start';
  static const STOP_FIELD = 'stop';

  final int start;
  final int stop;

  CatchupInfo(
      {required String id,
      required List<String> groups,
      required int iarc,
      required bool favorite,
      required int recent,
      required int interruptTime,
      required List<String> parts,
      required List<MetaUrl> meta,
      required List<Subtitle> subtitles,
      required UtcTimeMsec createdDate,
      required this.start,
      required this.stop,
      PricePack? price,
      String? pid,
      required EpgInfo epg})
      : super(
            id: id,
            groups: groups,
            iarc: iarc,
            favorite: favorite,
            recent: recent,
            interruptTime: interruptTime,
            parts: parts,
            meta: meta,
            subtitles: subtitles,
            createdDate: createdDate,
            price: price,
            epg: epg,
            pid: pid);

  @override
  CatchupInfo copy() {
    return CatchupInfo(
        id: id,
        groups: groups,
        iarc: iarc,
        favorite: favorite(),
        recent: recentTime(),
        interruptTime: interruptTime(),
        epg: epg,
        parts: parts,
        start: start,
        stop: stop,
        meta: meta,
        subtitles: subtitles,
        createdDate: createdDate,
        price: price,
        pid: pid);
  }

  factory CatchupInfo.fromJson(Map<String, dynamic> json) {
    final base = ChannelInfo.fromJson(json);
    final start = json[START_FIELD];
    final stop = json[STOP_FIELD];
    return CatchupInfo(
        id: base.id,
        groups: base.groups,
        iarc: base.iarc,
        favorite: base.favorite(),
        recent: base.recentTime(),
        interruptTime: base.interruptTime(),
        epg: base.epg,
        parts: base.parts,
        meta: base.meta,
        subtitles: base.subtitles,
        createdDate: base.createdDate,
        price: base.price,
        pid: base.pid,
        start: start,
        stop: stop);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> base = super.toJson();
    base[START_FIELD] = start;
    base[STOP_FIELD] = stop;
    return base;
  }

  int get durationMsec {
    return stop - start;
  }
}
