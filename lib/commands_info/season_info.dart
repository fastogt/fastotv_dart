import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/commands_info/vod_info.dart';
import 'package:fastotv_dart/utils.dart';

class SeasonInfo implements IDisplayContentInfo {
  static const _ID_FIELD = 'id';
  static const _NAME_FIELD = 'name';
  static const _BACKGROUND_FIELD = 'background_url';
  static const _ICON_FIELD = 'icon';
  static const _DESCRIPTION_FIELD = 'description';
  static const _SEASON_FIELD = 'season';
  static const _EPISODES_FIELD = 'episodes';
  static const _CREATED_DATE_FIELD = 'created_date';
  static const _PID_FIELD = 'pid';

  final String id;
  String name;
  String background;
  String previewIcon;
  String description;
  int season;
  List<String> episodes;

  final UtcTimeMsec createdDate;

  String? pid;

  @override
  String displayName() {
    return name;
  }

  @override
  String icon() {
    return previewIcon;
  }

  SeasonInfo(
      {required this.id,
      required this.name,
      required this.background,
      required this.previewIcon,
      required this.description,
      required this.season,
      required this.episodes,
      required this.createdDate,
      this.pid});

  SeasonInfo copy() {
    return SeasonInfo(
        id: id,
        name: name,
        background: background,
        previewIcon: previewIcon,
        description: description,
        season: season,
        episodes: episodes,
        createdDate: createdDate,
        pid: pid);
  }

  factory SeasonInfo.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final name = json[_NAME_FIELD];
    final background = json[_BACKGROUND_FIELD];
    final icon = json[_ICON_FIELD];
    final description = json[_DESCRIPTION_FIELD];
    final season = json[_SEASON_FIELD];
    final List<String> episodes = json[_EPISODES_FIELD].cast<String>();
    final createdDate = json[_CREATED_DATE_FIELD];
    String? pid;
    if (json.containsKey(_PID_FIELD)) {
      pid = json[_PID_FIELD];
    }
    return SeasonInfo(
        id: id,
        name: name,
        background: background,
        previewIcon: icon,
        description: description,
        season: season,
        episodes: episodes,
        createdDate: createdDate,
        pid: pid);
  }

  Map<String, dynamic> toJson() {
    final result = {
      _ID_FIELD: id,
      _NAME_FIELD: name,
      _BACKGROUND_FIELD: background,
      _ICON_FIELD: icon,
      _DESCRIPTION_FIELD: description,
      _SEASON_FIELD: season,
      _EPISODES_FIELD: episodes,
      _CREATED_DATE_FIELD: createdDate
    };
    if (pid != null) {
      result[_PID_FIELD] = pid!;
    }
    return result;
  }

  List<VodInfo> episodesInfos(List<VodInfo> cache) {
    final List<VodInfo> _episodes = [];
    for (final eid in episodes) {
      for (final episode in cache) {
        if (eid == episode.id) {
          _episodes.add(episode);
          break;
        }
      }
    }
    return _episodes;
  }
}
