import 'package:fastotv_dart/commands_info.dart';

class OttPackageInfo extends PackagePublic {
  static const _STREAMS_FIELD = 'streams';
  static const _VODS_FIELD = 'vods';
  static const _SERIALS_FIELD = 'serials';

  List<ChannelInfo> streams;
  List<VodInfo> vods;
  List<SerialInfo> serials;

  OttPackageInfo(
      {String? id,
      required String name,
      required String description,
      required String backgroundUrl,
      PricePack? price,
      required this.streams,
      required this.vods,
      required this.serials})
      : super(
            id: id,
            name: name,
            description: description,
            price: price,
            backgroundUrl: backgroundUrl);

  OttPackageInfo.copy({required OttPackageInfo src})
      : streams = src.streams,
        vods = src.vods,
        serials = src.serials,
        super(
            id: src.id,
            name: src.name,
            description: src.description,
            backgroundUrl: src.backgroundUrl,
            price: src.price);

  @override
  OttPackageInfo copy() {
    return OttPackageInfo.copy(src: this);
  }

  factory OttPackageInfo.fromJson(Map<String, dynamic> json) {
    final base = PackagePublic.fromJson(json);
    final List<dynamic> resultChannels = json[_STREAMS_FIELD];
    final List<ChannelInfo> streams = [];
    for (final element in resultChannels) {
      final lc = ChannelInfo.fromJson(element);
      streams.add(lc);
    }

    final List<dynamic> resultVods = json[_VODS_FIELD];
    final List<VodInfo> vods = [];
    for (final element in resultVods) {
      final lc = VodInfo.fromJson(element);
      vods.add(lc);
    }

    final List<dynamic> resultSerials = json[_SERIALS_FIELD];
    final List<SerialInfo> serials = [];
    for (final element in resultSerials) {
      final lc = SerialInfo.fromJson(element);
      serials.add(lc);
    }
    return OttPackageInfo(
        id: base.id,
        name: base.name,
        price: base.price,
        description: base.description,
        backgroundUrl: base.backgroundUrl,
        streams: streams,
        vods: vods,
        serials: serials);
  }

  @override
  Map<String, dynamic> toJson() {
    final List<Map<String, dynamic>> vodsResult = [];
    for (final element in vods) {
      vodsResult.add(element.toJson());
    }

    final List<Map<String, dynamic>> streamsResult = [];
    for (final element in streams) {
      streamsResult.add(element.toJson());
    }

    final List<Map<String, dynamic>> serialsResult = [];
    for (final element in serials) {
      serialsResult.add(element.toJson());
    }

    final base = super.toJson();
    base[_STREAMS_FIELD] = streamsResult;
    base[_VODS_FIELD] = vodsResult;
    base[_SERIALS_FIELD] = serialsResult;
    return base;
  }
}

class PackageInfo extends OttPackageInfo {
  static const _SERIES_FIELD = 'episodes';
  static const _SEASONS_FIELD = 'seasons';
  static const _CATCHUPS_FIELD = 'catchups';

  List<VodInfo> series;
  List<CatchupInfo> catchups;
  List<SeasonInfo> seasons;

  PackageInfo(
      {String? id,
      required String name,
      PricePack? price,
      required String description,
      required String backgroundUrl,
      required List<ChannelInfo> streams,
      required List<VodInfo> vods,
      required List<SerialInfo> serials,
      required this.catchups,
      required this.series,
      required this.seasons})
      : super(
            id: id,
            name: name,
            price: price,
            description: description,
            backgroundUrl: backgroundUrl,
            streams: streams,
            vods: vods,
            serials: serials);

  @override
  PackageInfo copy() {
    final base = super.copy();
    return PackageInfo(
        id: base.id,
        name: base.name,
        description: base.description,
        backgroundUrl: base.backgroundUrl,
        streams: base.streams,
        catchups: catchups,
        vods: base.vods,
        series: series,
        seasons: seasons,
        serials: base.serials);
  }

  factory PackageInfo.fromJson(Map<String, dynamic> json) {
    final base = OttPackageInfo.fromJson(json);

    final List<dynamic> resultCatchups = json[_CATCHUPS_FIELD];
    final List<CatchupInfo> catchups = [];
    for (final element in resultCatchups) {
      final lc = CatchupInfo.fromJson(element);
      catchups.add(lc);
    }

    final List<dynamic> resultSeries = json[_SERIES_FIELD];
    final List<VodInfo> series = [];
    for (final element in resultSeries) {
      final lc = VodInfo.fromJson(element);
      series.add(lc);
    }

    final List<dynamic> resultSeasons = json[_SEASONS_FIELD];
    final List<SeasonInfo> seasons = [];
    for (final element in resultSeasons) {
      final lc = SeasonInfo.fromJson(element);
      seasons.add(lc);
    }

    return PackageInfo(
        id: base.id,
        name: base.name,
        price: base.price,
        description: base.description,
        backgroundUrl: base.backgroundUrl,
        streams: base.streams,
        catchups: catchups,
        vods: base.vods,
        series: series,
        seasons: seasons,
        serials: base.serials);
  }

  @override
  Map<String, dynamic> toJson() {
    final List<Map<String, dynamic>> catchupsResult = [];
    for (final element in catchups) {
      catchupsResult.add(element.toJson());
    }
    final List<Map<String, dynamic>> seriesResult = [];
    for (final element in series) {
      seriesResult.add(element.toJson());
    }

    final List<Map<String, dynamic>> seasonsResult = [];
    for (final element in seasons) {
      seasonsResult.add(element.toJson());
    }

    final Map<String, dynamic> result = super.toJson();

    result[_CATCHUPS_FIELD] = catchupsResult;
    result[_SERIES_FIELD] = seriesResult;
    result[_SEASONS_FIELD] = seasonsResult;
    return result;
  }
}
