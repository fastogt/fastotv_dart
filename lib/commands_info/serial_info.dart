import 'package:fastotv_dart/commands_info/season_info.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/utils.dart';

class SerialInfo extends IDisplayContentInfo {
  static const _ID_FIELD = 'id';
  static const _NAME_FIELD = 'name';
  static const _BACKGROUND_FIELD = 'background_url';
  static const _ICON_FIELD = 'icon';
  static const _IARC_FIELD = 'iarc';
  static const _GROUPS_FIELD = 'groups';
  static const _DESCRIPTION_FIELD = 'description';
  static const _SEASONS_FIELD = 'seasons';
  static const _PRICE_FIELD = 'price';
  static const _PRIME_DATE_FIELD = 'prime_date';
  static const _CREATED_DATE_FIELD = 'created_date';
  static const _USER_SCORE_FIELD = 'user_score';
  static const _COUNTRY_FIELD = 'country';
  static const _DIRECTORS_FIELD = 'directors';
  static const _CAST_FIELD = 'cast';
  static const _PRODUCTION_FIELD = 'production';
  static const _GENRES_FIELD = 'genres';
  static const _FAVORITE_FIELD = 'favorite';
  static const _RECENT_FIELD = 'recent';

  static const _PID_FIELD = 'pid';

  final String id;
  String name;
  String background;
  String previewIcon;
  List<String> groups;
  int iarc;
  String description;
  List<String> seasons;
  UtcTimeMsec primeDate;

  double userScore;
  String country;
  List<String> directors; // ['Travolta', 'Mike']
  List<String> cast; // ['Jhonny Dep', 'Arnold']
  List<String> production; // ['WB', 'Netflix']
  List<String> genres; // ['Drama', 'Comedy']
  final UtcTimeMsec createdDate;

  bool _favorite;
  int _recent;

  PricePack? price;
  String? pid;

  SerialInfo(
      {required this.id,
      required this.name,
      required this.background,
      required this.previewIcon,
      required this.groups,
      required this.iarc,
      required this.description,
      required this.seasons,
      required this.primeDate,
      required this.createdDate,
      required this.userScore,
      required this.country,
      required this.directors,
      required this.cast,
      required this.production,
      required this.genres,
      required bool favorite,
      required int recent,
      this.price,
      this.pid})
      : _favorite = favorite,
        _recent = recent;

  SerialInfo copy() {
    return SerialInfo(
        id: id,
        name: name,
        background: background,
        previewIcon: previewIcon,
        groups: groups,
        iarc: iarc,
        description: description,
        seasons: seasons,
        primeDate: primeDate,
        price: price,
        createdDate: createdDate,
        userScore: userScore,
        country: country,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres,
        favorite: _favorite,
        recent: _recent,
        pid: pid);
  }

  @override
  String displayName() {
    return name;
  }

  @override
  String icon() {
    return previewIcon;
  }

  bool favorite() {
    return _favorite;
  }

  void setFavorite(bool value) {
    _favorite = value;
  }

  factory SerialInfo.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final name = json[_NAME_FIELD];
    final background = json[_BACKGROUND_FIELD];
    final previewIcon = json[_ICON_FIELD];
    final List<String> groups = json[_GROUPS_FIELD].cast<String>();
    final iarc = json[_IARC_FIELD];
    final description = json[_DESCRIPTION_FIELD];
    final List<String> seasons = json[_SEASONS_FIELD].cast<String>();
    final createdDate = json[_CREATED_DATE_FIELD];
    final primeDate = json[_PRIME_DATE_FIELD];
    final userScore = (json[_USER_SCORE_FIELD] as num).toDouble();
    final country = json[_COUNTRY_FIELD];
    final directors = json[_DIRECTORS_FIELD].cast<String>();
    final cast = json[_CAST_FIELD].cast<String>();
    final production = json[_PRODUCTION_FIELD].cast<String>();
    final genres = json[_GENRES_FIELD].cast<String>();
    final favorite = json[_FAVORITE_FIELD];
    final recent = json[_RECENT_FIELD];
    String? pid;
    if (json.containsKey(_PID_FIELD)) {
      pid = json[_PID_FIELD];
    }
    PricePack? price;
    if (json.containsKey(_PRICE_FIELD)) {
      price = PricePack.fromJson(json[_PRICE_FIELD]);
    }
    return SerialInfo(
        id: id,
        name: name,
        background: background,
        previewIcon: previewIcon,
        groups: groups,
        iarc: iarc,
        description: description,
        seasons: seasons,
        primeDate: primeDate,
        price: price,
        createdDate: createdDate,
        userScore: userScore,
        country: country,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres,
        favorite: favorite,
        recent: recent,
        pid: pid);
  }

  Map<String, dynamic> toJson() {
    final result = {
      _ID_FIELD: id,
      _NAME_FIELD: name,
      _BACKGROUND_FIELD: background,
      _ICON_FIELD: previewIcon,
      _GROUPS_FIELD: groups,
      _IARC_FIELD: iarc,
      _DESCRIPTION_FIELD: description,
      _SEASONS_FIELD: seasons,
      _PRIME_DATE_FIELD: primeDate,
      _CREATED_DATE_FIELD: createdDate,
      _USER_SCORE_FIELD: userScore,
      _COUNTRY_FIELD: country,
      _GENRES_FIELD: genres,
      _PRODUCTION_FIELD: production,
      _CAST_FIELD: cast,
      _DIRECTORS_FIELD: directors,
      _FAVORITE_FIELD: _favorite,
      _RECENT_FIELD: _recent,
    };
    if (pid != null) {
      result[_PID_FIELD] = pid!;
    }
    if (price != null) {
      result[_PRICE_FIELD] = price!.toJson();
    }
    return result;
  }

  List<SeasonInfo> seasonsInfos(List<SeasonInfo> cache) {
    final List<SeasonInfo> _seasons = [];
    for (final sid in seasons) {
      for (final season in cache) {
        if (sid == season.id) {
          _seasons.add(season);
          break;
        }
      }
    }
    return _seasons;
  }
}
