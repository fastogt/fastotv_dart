class PlatformType {
  const PlatformType._(this.os);

  final String os;

  static const String _WEB = 'web';
  static const String _ANDROID = 'android';
  static const String _IOS = 'ios';
  static const String _WINDOWS = 'windows';
  static const String _LINUX = 'linux';
  static const String _MACOSX = 'macosx';
  static const String _APPLE_TV = 'apple_tv';
  static const String _ROKU = 'roku';
  static const String _WEBOS = 'webos';
  static const String _TIZEN = 'tizen';
  static const String _UNKNOWN = 'unknown';

  static const PlatformType WEB = PlatformType._(_WEB);
  static const PlatformType ANDROID = PlatformType._(_ANDROID);
  static const PlatformType IOS = PlatformType._(_IOS);
  static const PlatformType WINDOWS = PlatformType._(_WINDOWS);
  static const PlatformType LINUX = PlatformType._(_LINUX);
  static const PlatformType MACOSX = PlatformType._(_MACOSX);
  static const PlatformType APPLE_TV = PlatformType._(_APPLE_TV);
  static const PlatformType ROKU = PlatformType._(_ROKU);
  static const PlatformType WEBOS = PlatformType._(_WEBOS);
  static const PlatformType TIZEN = PlatformType._(_TIZEN);
  static const PlatformType UNKNOWN = PlatformType._(_UNKNOWN);

  static List<PlatformType> get values =>
      [WEB, ANDROID, IOS, WINDOWS, LINUX, MACOSX, APPLE_TV, ROKU, WEBOS, TIZEN, UNKNOWN];

  factory PlatformType.fromString(String platform) {
    final value = platform.toLowerCase();
    if (value == _WEB) {
      return WEB;
    } else if (value == _ANDROID) {
      return ANDROID;
    } else if (value == _IOS) {
      return IOS;
    } else if (value == _WINDOWS) {
      return WINDOWS;
    } else if (value == _LINUX) {
      return LINUX;
    } else if (value == _MACOSX || value == 'darwin') {
      return MACOSX;
    } else if (value == _APPLE_TV) {
      return APPLE_TV;
    } else if (value == _ROKU) {
      return ROKU;
    } else if (value == _WEBOS) {
      return WEBOS;
    } else if (value == _TIZEN) {
      return TIZEN;
    }
    return UNKNOWN;
  }

  @override
  String toString() {
    return os;
  }

  String toHumanReadable() {
    if (os == _WEB) {
      return 'Web';
    } else if (os == _ANDROID) {
      return 'Android';
    } else if (os == _IOS) {
      return 'iOS';
    } else if (os == _WINDOWS) {
      return 'Windows';
    } else if (os == _LINUX) {
      return 'Linux';
    } else if (os == _MACOSX) {
      return 'Mac OS X';
    } else if (os == _APPLE_TV) {
      return 'Apple TV';
    } else if (os == _ROKU) {
      return 'Roku';
    } else if (os == _WEBOS) {
      return 'WebOS';
    } else if (os == _TIZEN) {
      return 'Tizen';
    }
    return 'Unknown';
  }
}
