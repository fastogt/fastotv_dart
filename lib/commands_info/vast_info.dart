import 'package:fastotv_dart/commands_info/iadservice_info.dart';

class Vast extends IAdService {
  static const VAST_FIELD = 'vast';

  VastData vast;

  Vast({required this.vast}) : super(type: AdType.VAST);

  Vast.createDefault()
      : vast = VastData.createDefault(),
        super(type: AdType.VAST);

  @override
  Vast copy() {
    return Vast(vast: vast.copy());
  }

  factory Vast.fromJson(Map<String, dynamic> json) {
    return Vast(vast: VastData.fromJson(json[VAST_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[VAST_FIELD] = vast.toJson();
    return result;
  }
}

class VastData {
  static const _URL_FIELD = 'url';

  String url;

  VastData({required this.url});

  factory VastData.createDefault() {
    return VastData(url: 'https://crocott.com');
  }

  VastData copy() {
    return VastData(url: url);
  }

  factory VastData.fromJson(Map<String, dynamic> json) {
    return VastData(url: json[_URL_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final result = {_URL_FIELD: url};
    return result;
  }
}
