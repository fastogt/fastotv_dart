import 'package:fastotv_dart/utils.dart';

abstract class IDisplayContentInfo {
  String displayName();
  String icon();
}

class PriceType {
  static const int _SUB_DAY_CONSTANT = 0;
  static const int _SUB_MONTH_CONSTANT = 1;
  static const int _SUB_YEAR_CONSTANT = 2;
  static const int _LIFE_TIME_CONSTANT = 3;

  final int _value;

  const PriceType._(this._value);

  int compareTo(PriceType status) {
    return _value.compareTo(status._value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _SUB_DAY_CONSTANT) {
      return 'Day';
    } else if (_value == _SUB_MONTH_CONSTANT) {
      return 'Month';
    } else if (_value == _SUB_YEAR_CONSTANT) {
      return 'Year';
    }

    assert(_value == _LIFE_TIME_CONSTANT);
    return 'Unlimited';
  }

  factory PriceType.fromInt(int mode) {
    if (mode == _SUB_DAY_CONSTANT) {
      return PriceType.SUB_DAY;
    } else if (mode == _SUB_MONTH_CONSTANT) {
      return PriceType.SUB_MONTH;
    } else if (mode == _SUB_YEAR_CONSTANT) {
      return PriceType.SUB_YEAR;
    } else if (mode == _LIFE_TIME_CONSTANT) {
      return PriceType.LIFE_TIME;
    }

    throw 'Unknown price type: $mode';
  }

  static List<PriceType> get values => [SUB_DAY, SUB_MONTH, SUB_YEAR, LIFE_TIME];

  static const PriceType SUB_DAY = PriceType._(_SUB_DAY_CONSTANT);
  static const PriceType SUB_MONTH = PriceType._(_SUB_MONTH_CONSTANT);
  static const PriceType SUB_YEAR = PriceType._(_SUB_YEAR_CONSTANT);
  static const PriceType LIFE_TIME = PriceType._(_LIFE_TIME_CONSTANT);
}

class PricePack {
  static const _AMOUNT_FIELD = 'amount';
  static const _TYPE_FIELD = 'type';
  static const _CURRENCY_FIELD = 'currency';

  double price;
  PriceType type;
  Currency currency;

  PricePack({required this.price, required this.type, required this.currency});

  PricePack copy() {
    return PricePack(price: price, type: type, currency: currency);
  }

  factory PricePack.fromJson(Map<String, dynamic> json) {
    final double price = (json[_AMOUNT_FIELD] as num).toDouble();
    final int type = json[_TYPE_FIELD];
    final String currency = json[_CURRENCY_FIELD];
    return PricePack(
        price: price, type: PriceType.fromInt(type), currency: Currency.fromString(currency));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      _AMOUNT_FIELD: price,
      _TYPE_FIELD: type.toInt(),
      _CURRENCY_FIELD: currency.type
    };
    return result;
  }
}

class Currency {
  static const CURRENCY_USD = 'USD';
  static const CURRENCY_EUR = 'EUR';
  static const CURRENCY_RUB = 'RUB';
  static const CURRENCY_BYN = 'BYN';

  final String type;

  const Currency(this.type);

  Currency copy() {
    return Currency(type);
  }

  String toHumanReadable() {
    if (type == CURRENCY_USD) {
      return 'American Dollar ($CURRENCY_USD)';
    } else if (type == CURRENCY_EUR) {
      return 'European Euro ($CURRENCY_EUR)';
    } else if (type == CURRENCY_RUB) {
      return 'Russian Ruble ($CURRENCY_RUB)';
    } else if (type == CURRENCY_BYN) {
      return 'Belarusian Ruble ($CURRENCY_BYN)';
    }
    return 'American Dollar ($CURRENCY_USD)';
  }

  factory Currency.fromString(String value) {
    if (value == CURRENCY_USD) {
      return USD;
    } else if (value == CURRENCY_EUR) {
      return EUR;
    } else if (value == CURRENCY_RUB) {
      return RUB;
    } else if (value == CURRENCY_BYN) {
      return BYN;
    }

    throw 'Unknown currency value: $value';
  }

  static List<Currency> get values => [USD, EUR, RUB, BYN];

  static const Currency USD = Currency(CURRENCY_USD);
  static const Currency EUR = Currency(CURRENCY_EUR);
  static const Currency RUB = Currency(CURRENCY_RUB);
  static const Currency BYN = Currency(CURRENCY_BYN);
}

class WsMode {
  const WsMode._(this._value);

  static const int _OTT = 0;
  static const int _IPTV = 1;
  static const int _CCTV = 2;
  static const int _DRONE = 3;

  static const WsMode OTT = WsMode._(_OTT);
  static const WsMode IPTV = WsMode._(_IPTV);
  static const WsMode CCTV = WsMode._(_CCTV);
  static const WsMode DRONE = WsMode._(_DRONE);

  final int _value;

  static List<WsMode> get values => [OTT, IPTV, CCTV, DRONE];

  String toHumanReadable() {
    if (_value == _OTT) {
      return 'OTT';
    } else if (_value == _IPTV) {
      return 'IPTV';
    } else if (_value == _CCTV) {
      return 'CCTV';
    } else if (_value == _DRONE) {
      return 'DRONE';
    }
    return 'OTT';
  }

  factory WsMode.fromInt(int mode) {
    if (mode == _OTT) {
      return WsMode.OTT;
    } else if (mode == _IPTV) {
      return WsMode.IPTV;
    } else if (mode == _CCTV) {
      return WsMode.CCTV;
    } else if (mode == _DRONE) {
      return WsMode.DRONE;
    }

    throw 'Unknown ws mode: $mode';
  }

  int toInt() {
    return _value;
  }
}

class LatLng {
  static const _LATITUDE_FIELD = 'lat';
  static const _LONGITUDE_FIELD = 'lng';

  double latitude;
  double longitude;

  LatLng({required this.latitude, required this.longitude});

  factory LatLng.createDefault() {
    return LatLng(latitude: 40, longitude: 74);
  }

  LatLng copy() {
    return LatLng(latitude: latitude, longitude: longitude);
  }

  factory LatLng.fromJson(Map<String, dynamic> json) {
    final double latitude = (json[_LATITUDE_FIELD] as num).toDouble();
    final double longitude = (json[_LONGITUDE_FIELD] as num).toDouble();
    return LatLng(latitude: latitude, longitude: longitude);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_LATITUDE_FIELD: latitude, _LONGITUDE_FIELD: longitude};
    return result;
  }
}

class Comment {
  static const _TEXT_FIELD = 'text';
  static const _CREATED_DATE_FIELD = 'created_date';

  String text;
  UtcTimeMsec createdDate;

  Comment({required this.text, required this.createdDate});

  Comment copy() {
    return Comment(text: text, createdDate: createdDate);
  }

  factory Comment.fromJson(Map<String, dynamic> json) {
    final text = json[_TEXT_FIELD];
    final created = json[_CREATED_DATE_FIELD];
    return Comment(text: text, createdDate: created);
  }

  Map<String, dynamic> toJson() {
    return {_TEXT_FIELD: text, _CREATED_DATE_FIELD: createdDate};
  }
}
