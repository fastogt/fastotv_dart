class PlayingUrlType {
  const PlayingUrlType._(this._value);

  static const int _UNKNOWN = 0;
  static const int _WHEP = 1;
  static const int _YOUTUBE = 2;
  static const int _EMBED = 3;

  static const String UNKNOWN_TYPE = 'UNKNOWN';
  static const String WHEP_TYPE = 'WHEP';
  static const String YOUTUBE_TYPE = 'YOUTUBE';
  static const String EMBED_TYPE = 'EMBED';

  static const PlayingUrlType UNKNOWN = PlayingUrlType._(_UNKNOWN);
  static const PlayingUrlType WHEP = PlayingUrlType._(_WHEP);
  static const PlayingUrlType YOUTUBE = PlayingUrlType._(_YOUTUBE);
  static const PlayingUrlType EMBED = PlayingUrlType._(_EMBED);

  final int _value;

  static List<PlayingUrlType> get values => [UNKNOWN, WHEP, YOUTUBE, EMBED];

  static List<String> get stringValues => [UNKNOWN_TYPE, WHEP_TYPE, YOUTUBE_TYPE, EMBED_TYPE];

  int compareTo(PlayingUrlType status) {
    return _value.compareTo(status._value);
  }

  String toHumanReadable() {
    if (_value == _UNKNOWN) {
      return UNKNOWN_TYPE;
    } else if (_value == _WHEP) {
      return WHEP_TYPE;
    } else if (_value == _YOUTUBE) {
      return YOUTUBE_TYPE;
    } else if (_value == _EMBED) {
      return EMBED_TYPE;
    }

    return UNKNOWN_TYPE;
  }

  factory PlayingUrlType.fromInt(int type) {
    if (type == _UNKNOWN) {
      return PlayingUrlType.UNKNOWN;
    } else if (type == _WHEP) {
      return PlayingUrlType.WHEP;
    } else if (type == _YOUTUBE) {
      return PlayingUrlType.YOUTUBE;
    } else if (type == _EMBED) {
      return PlayingUrlType.EMBED;
    }

    throw 'Unknown playing url type: $type';
  }

  int toInt() {
    return _value;
  }
}

class PlayingUrl {
  static const _URL_FIELD = 'url';
  static const _TYPE_FIELD = 'type';

  String url;
  PlayingUrlType type;

  PlayingUrl({required this.url, this.type = PlayingUrlType.UNKNOWN});

  factory PlayingUrl.fromJson(Map<String, dynamic> json) {
    final url = json[_URL_FIELD];
    final type = json[_TYPE_FIELD];
    return PlayingUrl(url: url, type: PlayingUrlType.fromInt(type));
  }

  PlayingUrl copy() {
    return PlayingUrl(url: url, type: type);
  }

  Map<String, dynamic> toJson() {
    return {_URL_FIELD: url, _TYPE_FIELD: type.toInt()};
  }
}
