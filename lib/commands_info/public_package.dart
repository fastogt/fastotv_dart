import 'package:fastotv_dart/commands_info/types.dart';

class PackagePublic {
  static const _ID_FIELD = 'id';
  static const _NAME_FIELD = 'name';
  static const _DESCRIPTION_FIELD = 'description';
  static const _BACKGROUND_URL_FIELD = 'background_url';
  static const _PRICE_FIELD = 'price';

  final String? id; // if m3u list = null
  String name;
  String description;
  String backgroundUrl;
  PricePack? price;

  PackagePublic(
      {this.id,
      required this.name,
      required this.description,
      required this.backgroundUrl,
      this.price});

  PackagePublic.copy({required PackagePublic src})
      : id = src.id,
        name = src.name,
        description = src.description,
        backgroundUrl = src.backgroundUrl,
        price = src.price;

  PackagePublic copy() {
    return PackagePublic.copy(src: this);
  }

  factory PackagePublic.fromJson(Map<String, dynamic> json) {
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    final name = json[_NAME_FIELD];
    final description = json[_DESCRIPTION_FIELD];
    final backgroundUrl = json[_BACKGROUND_URL_FIELD];
    PricePack? price;
    if (json.containsKey(_PRICE_FIELD)) {
      price = PricePack.fromJson(json[_PRICE_FIELD]);
    }

    return PackagePublic(
        id: id, name: name, description: description, backgroundUrl: backgroundUrl, price: price);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    result[_NAME_FIELD] = name;
    if (price != null) {
      result[_PRICE_FIELD] = price!.toJson();
    }
    result[_DESCRIPTION_FIELD] = description;
    result[_BACKGROUND_URL_FIELD] = backgroundUrl;
    return result;
  }
}
