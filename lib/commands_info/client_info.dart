import 'package:fastotv_dart/commands_info/platform.dart';

class OperationSystem {
  static const _NAME_FIELD = 'name';
  static const _VERSION_FIELD = 'version';
  static const _ARCH_FIELD = 'arch';

  final PlatformType name;
  final String version;
  final String arch;

  OperationSystem({required this.name, required this.version, required this.arch});

  factory OperationSystem.createUnknown() {
    return OperationSystem(name: PlatformType.UNKNOWN, version: 'Unknown', arch: 'Unknown');
  }

  OperationSystem copy() {
    return OperationSystem(name: name, version: version, arch: arch);
  }

  factory OperationSystem.fromJson(Map<String, dynamic> json) {
    final name = json[_NAME_FIELD];
    final version = json[_VERSION_FIELD];
    final arch = json[_ARCH_FIELD];
    return OperationSystem(name: PlatformType.fromString(name), version: version, arch: arch);
  }

  Map<String, dynamic> toJson() {
    return {_NAME_FIELD: name.toString(), _VERSION_FIELD: version, _ARCH_FIELD: arch};
  }
}

class OperationSystemInfo extends OperationSystem {
  static const RAM_TOTAL_FIELD = 'ram_total';
  static const RAM_FREE_FIELD = 'ram_free';

  final int ramTotal;
  final int ramFree;

  OperationSystemInfo(
      {required PlatformType name,
      required String version,
      required String arch,
      required this.ramTotal,
      required this.ramFree})
      : super(name: name, version: version, arch: arch);

  @override
  OperationSystemInfo copy() {
    return OperationSystemInfo(
        name: name, version: version, arch: arch, ramTotal: ramTotal, ramFree: ramFree);
  }

  @override
  Map<String, dynamic> toJson() {
    final base = super.toJson();
    base[RAM_TOTAL_FIELD] = ramTotal;
    base[RAM_FREE_FIELD] = ramFree;
    return base;
  }

  factory OperationSystemInfo.fromJson(Map<String, dynamic> json) {
    final base = OperationSystem.fromJson(json);
    final ramTotal = json[RAM_TOTAL_FIELD];
    final ramFree = json[RAM_FREE_FIELD];
    return OperationSystemInfo(
        name: base.name,
        version: base.version,
        arch: base.arch,
        ramTotal: ramTotal,
        ramFree: ramFree);
  }
}

class Project {
  static const String _CROCOTT = 'CrocOTT';
  static const String _TURTLEOTT = 'TurtleOTT';
  static const String _PYTHONOTT = 'PythonOTT';
  static const String _RAPTOROTT = 'RaptorOTT';
  static const String _VENOMOTT = 'VenomOTT';

  static const NAME_FIELD = 'name';
  static const VERSION_FILED = 'version';

  final String name;
  final String version;

  Project({required this.name, required this.version});
  Project.crocott({required this.version}) : name = _CROCOTT;
  Project.turtleott({required this.version}) : name = _TURTLEOTT;
  Project.pythonott({required this.version}) : name = _PYTHONOTT;
  Project.raptorott({required this.version}) : name = _RAPTOROTT;
  Project.venomott({required this.version}) : name = _VENOMOTT;

  Project copy() {
    return Project(name: name, version: version);
  }

  Map<String, dynamic> toJson() {
    return {NAME_FIELD: name, VERSION_FILED: version};
  }

  factory Project.fromJson(Map<String, dynamic> json) {
    final name = json[NAME_FIELD];
    final version = json[VERSION_FILED];
    return Project(name: name, version: version);
  }

  bool isRegistered() {
    final value = name.toLowerCase();
    if (value == _CROCOTT.toLowerCase()) {
      return true;
    } else if (value == _TURTLEOTT.toLowerCase()) {
      return true;
    } else if (value == _PYTHONOTT.toLowerCase()) {
      return true;
    } else if (value == _RAPTOROTT.toLowerCase()) {
      return true;
    } else if (value == _VENOMOTT.toLowerCase()) {
      return true;
    }
    return false;
  }
}

class ClientInfo {
  static const DEVICE_ID_FILED = 'id';
  static const OS_FILED = 'os';
  static const PROJECT_FILED = 'project';
  static const CPU_BRAND_FILED = 'cpu_brand';

  final String id;
  final Project project;
  final OperationSystemInfo os;
  final String cpuBrand;

  ClientInfo({required this.id, required this.project, required this.os, required this.cpuBrand});

  ClientInfo copy() {
    return ClientInfo(id: id, project: project, os: os, cpuBrand: cpuBrand);
  }

  Map<String, dynamic> toJson() {
    return {
      DEVICE_ID_FILED: id,
      PROJECT_FILED: project.toJson(),
      OS_FILED: os.toJson(),
      CPU_BRAND_FILED: cpuBrand
    };
  }
}
