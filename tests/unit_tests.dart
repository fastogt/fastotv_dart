import 'dart:io';

import 'package:fastotv_dart/m3u_parser.dart';
import 'package:fastotv_dart/m3u_to_channels.dart';
import 'package:test/test.dart';

void main() {
  test('UTC', () async {
    final File file = File('tests/ser.m3u');
    final m3uLink = await file.readAsString();

    final List<M3uEntry> m3u = await M3UParser(m3uLink).parseFromString() ?? [];
    final package = ChannelsFromFile(m3u).makePackage("name", "description");

    expect(package.streams.length, 3);
    expect(package.vods.length, 5);
    expect(package.series.length, 57);
    expect(package.seasons.length, 3);
    expect(package.serials.length, 1);
  });
}
